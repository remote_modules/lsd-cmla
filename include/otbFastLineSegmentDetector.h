/*
 * Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part a Remote Module for Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef FAST_LINE_SEGMENT_DETECTOR_H
#define FAST_LINE_SEGMENT_DETECTOR_H

#include "otbImage.h"
#include "otbVectorDataSource.h"
#include "otbVectorData.h"

namespace otb
{

template <class TInputImage, class TPrecision = double>
class ITK_EXPORT FastLineSegmentDetector : public VectorDataSource<otb::VectorData<TPrecision>>
{
public:
	typedef FastLineSegmentDetector                  Self;
	typedef VectorDataSource<VectorData<TPrecision>> Superclass;
	typedef itk::SmartPointer<Self>                  Pointer;
	typedef itk::SmartPointer<const Self>            ConstPointer;

	itkNewMacro(Self);

	itkTypeMacro(FastLineSegmentDetector, VectorDataSource);

	typedef TInputImage                           InputImageType;
    typedef typename InputImageType::Pointer      InputImagePointer;
    typedef typename InputImageType::ConstPointer InputImageConstPointer;
	typedef typename InputImageType::PixelType    InputPixelType;
	typedef typename InputImageType::IndexType    InputIndexType;
	typedef typename InputImageType::SizeType     InputSizeType;
	typedef typename InputImageType::RegionType   InputRegionType;
	typedef typename InputImageType::SpacingType  InputSpacingType;
	typedef typename InputImageType::PointType    InputOriginType;

	typedef VectorData<TPrecision>                VectorDataType;
	typedef typename VectorDataType::DataNodeType DataNodeType;
	typedef typename VectorDataType::LineType     LineType;
	typedef typename VectorDataType::PointType    PointType;
	typedef typename LineType::VertexType         VertexType;

	using Superclass::SetInput;
	virtual void SetInput(const InputImageType* input);
	virtual const InputImageType* GetInput(void);

    itkSetMacro(Scale, double);
    itkGetMacro(Scale, double);
    itkSetMacro(SigmaCoef, double);
    itkGetMacro(SigmaCoef, double);
    itkSetMacro(Quant, double);
    itkGetMacro(Quant, double);
    itkSetMacro(AngTh, double);
    itkGetMacro(AngTh, double);
    itkSetMacro(LogEps, double);
    itkGetMacro(LogEps, double);
    itkSetMacro(DensityTh, double);
    itkGetMacro(DensityTh, double);
    itkSetMacro(NBins, unsigned int);
    itkGetMacro(NBins, unsigned int);

protected:
	FastLineSegmentDetector();
	~FastLineSegmentDetector() override {}

	void GenerateInputRequestedRegion() override;

	void GenerateData() override;

	void PrintSelf(std::ostream& os, itk::Indent indent) const override;

	private:
	FastLineSegmentDetector(const Self&) = delete;
	void operator=(const Self&) = delete;

	// Gaussian scaling
    double m_Scale;
    // Ratio for sigma of gaussian filter
    double m_SigmaCoef;
    // Gradient quantization error
    double m_Quant;
    // Gradient angle tolerance
    double m_AngTh;
    // Detection threshold
    double m_LogEps;
    // Minimal density of regions
    double m_DensityTh;
    // Number of bins in ordering
    unsigned int m_NBins;
};

} // namespace otb


#include "otbFastLineSegmentDetector.txx"

#endif
