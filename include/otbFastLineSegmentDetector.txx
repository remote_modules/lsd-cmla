/*
 * Copyright (C) 2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part a Remote Module for Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
extern "C"{
  #include "lsd.h"
}
#include "otbFastLineSegmentDetector.h"
#include "itkImageRegionIterator.h"
#include "itkImageConstIterator.h"

namespace otb
{

template <class TInputImage, class TPrecision>
FastLineSegmentDetector<TInputImage, TPrecision>
::FastLineSegmentDetector()
: m_Scale(0.)
, m_SigmaCoef(0.)
, m_Quant(0.)
, m_AngTh(0.)
, m_LogEps(0.)
, m_DensityTh(0.)
, m_NBins(0) {
    this->SetNumberOfRequiredInputs(1);
    this->SetNumberOfRequiredOutputs(1);
}

template <class TInputImage, class TPrecision>
void
FastLineSegmentDetector<TInputImage, TPrecision>
::SetInput(const InputImageType *input) {
    this->Superclass::SetNthInput(0, const_cast<InputImageType*>(input));
}

template <class TInputImage, class TPrecision>
const typename FastLineSegmentDetector<TInputImage, TPrecision>::InputImageType*
FastLineSegmentDetector<TInputImage, TPrecision>
::GetInput(void) {
    if (this->GetNumberOfInputs() < 1) {
        return ITK_NULLPTR;
    }
    return static_cast<const InputImageType *>(this->Superclass::GetInput(0));
}

template <class TInputImage, class TPrecision>
void
FastLineSegmentDetector<TInputImage, TPrecision>
::GenerateInputRequestedRegion(void) {
    // Call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();

    // Get pointers to the inputs
    typename InputImageType::Pointer inputImage = const_cast<InputImageType *> (this->GetInput());
    if ( !inputImage ) {
        return;
    }

    // The input is necessarily the largest possible region.
    inputImage->SetRequestedRegionToLargestPossibleRegion();
}

template <class TInputImage, class TPrecision>
void
FastLineSegmentDetector<TInputImage, TPrecision>
::GenerateData() {
    typename InputImageType::Pointer inputImage = const_cast<InputImageType *> (this->GetInput());
    if(inputImage->GetRequestedRegion() != inputImage->GetLargestPossibleRegion()) {
        itkExceptionMacro(<< "Not streamed filter. ERROR : requested region is not the largest possible region.");
    }

    InputRegionType inputRegion = inputImage->GetRequestedRegion();
    // Get the columns and lines number
    unsigned int inNbCol = inputRegion.GetSize()[0];
    unsigned int inNbLig = inputRegion.GetSize()[1];
    unsigned int i = 0;
    double* imageBuffer = new double[(size_t)(inNbCol*inNbLig)];

    // Extract image buffer
    typedef itk::ImageRegionConstIterator<InputImageType> InputIterator;
    InputIterator inputIt(inputImage, inputRegion);
    inputIt.GoToBegin();
    // Copy the input image
    for(; !inputIt.IsAtEnd(); i++, ++inputIt) {
        imageBuffer[i] = (double)inputIt.Get();
    }

    // Detect segments
    int n = 0;
    int regXNotUsed = 0;
    int regYNotUsed = 0;
    double* segments = LineSegmentDetection(&n, imageBuffer, inNbCol, inNbLig,
            m_Scale, m_SigmaCoef, m_Quant, m_AngTh, m_LogEps, m_DensityTh, m_NBins,
            NULL, &regXNotUsed, &regYNotUsed);

    // Fill output
    this->GetOutput(0)->SetMetaDataDictionary(this->GetInput()->GetMetaDataDictionary());
    // Retrieving root node
    typename DataNodeType::Pointer root = this->GetOutput(0)->GetDataTree()->GetRoot()->Get();
    // Create the document node
    typename DataNodeType::Pointer document = DataNodeType::New();
    document->SetNodeType(otb::DOCUMENT);
    // Adding the layer to the data tree
    this->GetOutput(0)->GetDataTree()->Add(document, root);
    // Create the folder node
    typename DataNodeType::Pointer folder = DataNodeType::New();
    folder->SetNodeType(otb::FOLDER);
    // Adding the layer to the data tree
    this->GetOutput(0)->GetDataTree()->Add(folder, document);
    this->GetOutput(0)->SetProjectionRef(this->GetInput()->GetProjectionRef());
    InputSpacingType spacing = this->GetInput()->GetSignedSpacing();
    InputOriginType  origin  = this->GetInput()->GetOrigin();
    InputIndexType   index   = inputRegion.GetIndex();

    for(int i = 0; i < n; i++) {
        VertexType start, end;
        start[0] = origin[0] + (index[0] + segments[i * 7])     * spacing[0];
        start[1] = origin[1] + (index[1] + segments[i * 7 + 1]) * spacing[1];
        end[0]   = origin[0] + (index[0] + segments[i * 7 + 2]) * spacing[0];
        end[1]   = origin[1] + (index[1] + segments[i * 7 + 3]) * spacing[1];

        typename DataNodeType::Pointer CurrentGeometry = DataNodeType::New();
        CurrentGeometry->SetNodeId("FEATURE_LINE");
        CurrentGeometry->SetNodeType(otb::FEATURE_LINE);
        typename LineType::Pointer line = LineType::New();
        CurrentGeometry->SetLine(line);
        this->GetOutput(0)->GetDataTree()->Add(CurrentGeometry, folder);
        CurrentGeometry->GetLine()->AddVertex(start);
        CurrentGeometry->GetLine()->AddVertex(end);
    }

    // Release memory
    delete[] imageBuffer;
    free( (void *) segments );
}

template <class TInputImage, class TPrecision>
void
FastLineSegmentDetector<TInputImage, TPrecision>
::PrintSelf(std::ostream& os, itk::Indent indent) const {
    Superclass::PrintSelf(os, indent);

    os << indent << "Gaussian scaling: " << m_Scale << std::endl;
    os << indent << "Ratio for sigma of gaussian filter: " << m_SigmaCoef << std::endl;
    os << indent << "Gradient quantization error: " << m_Quant << std::endl;
    os << indent << "Gradient angle tolerance: " << m_AngTh << std::endl;
    os << indent << "Detection threshold: " << m_LogEps << std::endl;
    os << indent << "Minimal density of regions: " << m_DensityTh << std::endl;
    os << indent << "Number of bins in ordering: " << m_NBins << std::endl;
}

 } // namespace otb
