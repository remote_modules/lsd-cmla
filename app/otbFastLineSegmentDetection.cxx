/*
 * Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part a Remote Module for Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "otbImage.h"
#include "otbOGRDataSourceWrapper.h"
#include "otbOGRFeatureWrapper.h"
#include "otbMultiToMonoChannelExtractROI.h"

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbFastLineSegmentDetector.h"

namespace otb
{

//  FastLineSegmentDetection class is defined in Wrapper namespace.
namespace Wrapper
{

//  FastLineSegmentDetection class is derived from Application class.

class FastLineSegmentDetection : public Application
{
private:
  using PixelType              = float;
  using ImageType              = otb::Image<PixelType, 2>;
  using VectorImageType        = otb::VectorImage<PixelType>;
  using VectorImageDataType    = otb::VectorData<PixelType, 2, double>;
  using FLSDFilterType         = FastLineSegmentDetector<ImageType, PixelType>;
  using ExtractROIFilterType   = otb::MultiToMonoChannelExtractROI<PixelType, PixelType>;

  using TreeIteratorType       = itk::PreOrderTreeIterator<typename VectorImageDataType::DataTreeType>;
  using LineType               = VectorImageDataType::LineType;
  using VertexType             = LineType::VertexType;
  using VertexListType         = LineType::VertexListType;


public:
  // Class declaration is followed by \code{ITK} public types for the class, the superclass and
  // smart pointers.

  using Self         = FastLineSegmentDetection;
  using Superclass   = Application;
  using Pointer      = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;

  //  Following macros are necessary to respect ITK object factory mechanisms. Please report
  //  to \ref{sec:FilterConventions} for additional information.

  itkNewMacro(Self);
  itkTypeMacro(CMLALSD, otb::Application);

private:
  void DoInit() override
  {
    SetName("FastLineSegmentDetection");
    SetDescription("Linear-time Line Segment Detector giving subpixel accurate results");
    SetDocLongDescription("Linear-time Line Segment Detector giving subpixel accurate results ; \
It is designed to work on any digital image without parameter tuning. \
It controls its own number of false detections: On average, one false alarms is allowed per image. \
The method is based on Burns, Hanson, and Riseman's method, \
and uses an a-contrario validation approach according to Desolneux, Moisan, and Morel's theory. \
The version described here includes some further improvement over the one described in the original article.");
    SetDocLimitations("None");
    SetDocAuthors("Line Segment Detector (CMLA implementation) : Rafael Grompone von Gioi1, Jérémie Jakubowicz, \
Jean-Michel Morel, Gregory Randall. OTB integration : Julien Michel, Yannick Tanguy, Rémi Demortier");
    SetDocSeeAlso("Gioi, R., Jakubowicz, J., Morel, J.-M., Randall, G., 2010. LSD: A Fast Line Segment Detector \
with a False Detection Control. IEEE transactions on pattern analysis and machine intelligence, 32, 722-32.");
    AddDocTag(Tags::Filter);

    // Input data
    AddParameter(ParameterType_InputImage, "in", "Input image path");
    SetParameterDescription("in", "Input image path.");
    // Output data
    AddParameter(ParameterType_OutputFilename, "lsd", "Output lsd shapefile");
    SetParameterDescription("lsd", "Output file (*.shp) containing the detected segments.");

    // Parameters
    AddParameter(ParameterType_Float,"scale","Gaussian scaling");
    SetParameterDescription("scale","Scale image by Gaussian filter before processing.");
    SetDefaultParameterFloat("scale",0.8);
    SetMinimumParameterFloatValue("scale",0);

    AddParameter(ParameterType_Float,"sigmacoef","Ratio for sigma of gaussian filter");
    SetParameterDescription("sigmacoef","Sigma for Gaussian filter is computed as sigma_coef/scale.");
    SetDefaultParameterFloat("sigmacoef",0.6);
    SetMinimumParameterFloatValue("sigmacoef",0);

    AddParameter(ParameterType_Float,"quant","Gradient quantization error");
    SetParameterDescription("quant","Bound to quantization error on the gradient norm.");
    SetDefaultParameterFloat("quant",2);
    SetMinimumParameterFloatValue("quant",0);

    AddParameter(ParameterType_Float,"angth","Gradient angle tolerance");
    SetParameterDescription("angth","Gradient angle tolerance in degrees.");
    SetDefaultParameterFloat("angth",22.5);
    SetMinimumParameterFloatValue("angth",0);

    AddParameter(ParameterType_Float,"logeps","Detection threshold");
    SetParameterDescription("logeps","Detection threshold");
    SetDefaultParameterFloat("logeps",0);

    AddParameter(ParameterType_Float,"densityth","Minimal density of regions");
    SetParameterDescription("densityth","Minimal density of region points in a rectangle to be accepted.");
    SetDefaultParameterFloat("densityth",0.7);
    SetMinimumParameterFloatValue("densityth",0);
    SetMaximumParameterFloatValue("densityth",1);

    AddParameter(ParameterType_Int, "nbins", "Number of bins in ordering");
    SetParameterDescription("nbins", "Number of bins in ordering of gradient modulus.");
    SetDefaultParameterInt("nbins", 1024);
    SetMinimumParameterIntValue("nbins", 1);

    AddParameter(ParameterType_Int, "tilesizex", "Size of tiles in pixel (X-axis)");
    SetParameterDescription("tilesizex", "Size of tiles along the X-axis.");
    SetDefaultParameterInt("tilesizex", 500);
    SetMinimumParameterIntValue("tilesizex", 1);

    AddParameter(ParameterType_Int, "tilesizey", "Size of tiles in pixel (Y-axis)");
    SetParameterDescription("tilesizey", "Size of tiles along the Y-axis.");
    SetDefaultParameterInt("tilesizey", 500);
    SetMinimumParameterIntValue("tilesizey", 1);

    // Doc example parameter settings
    SetDocExampleParameterValue("in", "input_image.tif");
    SetDocExampleParameterValue("lsd", "output_vectorDB.shp");
    SetDocExampleParameterValue("scale", "0.8");
    SetDocExampleParameterValue("sigmacoef", "0.6");
    SetDocExampleParameterValue("quant", "2");
    SetDocExampleParameterValue("angth", "22.5");
    SetDocExampleParameterValue("logeps", "0");
    SetDocExampleParameterValue("densityth", "0.7");
    SetDocExampleParameterValue("nbins", "1024");
    SetDocExampleParameterValue("tilesizex", "500");
    SetDocExampleParameterValue("tilesizey", "500");
  }

  void DoUpdateParameters() override {}

  void DoExecute() override
  {
    unsigned long sizeTilesX = GetParameterInt("tilesizex");
    unsigned long sizeTilesY = GetParameterInt("tilesizey");

    //Acquisition of the input image dimensions
    VectorImageType::Pointer imageIn = GetParameterImage("in");
    imageIn->UpdateOutputInformation();
    unsigned long sizeImageX = imageIn->GetLargestPossibleRegion().GetSize()[0];
    unsigned long sizeImageY = imageIn->GetLargestPossibleRegion().GetSize()[1];
    unsigned int nbTilesX = sizeImageX/sizeTilesX + (sizeImageX%sizeTilesX > 0 ? 1 : 0);
    unsigned int nbTilesY = sizeImageY/sizeTilesY + (sizeImageY%sizeTilesY > 0 ? 1 : 0);

    otbAppLogINFO(<<"Number of tiles: "<<nbTilesX<<" x "<<nbTilesY);

    // Prepare output layer
    otb::ogr::DataSource::Pointer ogrDS;
    otb::ogr::Layer layer(NULL, false);

    OGRSpatialReference oSRS(imageIn->GetProjectionRef().c_str());
    std::vector<std::string> options;

    ogrDS = otb::ogr::DataSource::New(GetParameterString("lsd"), otb::ogr::DataSource::Modes::Overwrite);
    std::string layername = itksys::SystemTools::GetFilenameName(GetParameterString("lsd"));
    std::string extension = itksys::SystemTools::GetFilenameLastExtension(GetParameterString("lsd"));
    layername = layername.substr(0,layername.size()-extension.size());
    layer = ogrDS->CreateLayer(layername, &oSRS, wkbLineString, options);

    // Sums calculation per label
    unsigned int currentProgress = 0;
    unsigned long totalNbSegments = 0;
    std::list<OGRLineString> detectedSegments;

    for(unsigned int row = 0; row < nbTilesY; row++) {
      for(unsigned int column = 0; column < nbTilesX; column++) {
        double progress = 100 * static_cast<double>(column+nbTilesX*row)/static_cast<double>(nbTilesX*nbTilesY);

        while(progress >= currentProgress) {
          otbAppLogINFO(<< "Progress: " << currentProgress << "% (" << totalNbSegments << " found so far)");
          currentProgress+=10;
        }

        FLSDFilterType::Pointer lsdFilter = FLSDFilterType::New();
        lsdFilter->SetScale(GetParameterFloat("scale"));
        lsdFilter->SetSigmaCoef(GetParameterFloat("sigmacoef"));
        lsdFilter->SetQuant(GetParameterFloat("quant"));
        lsdFilter->SetAngTh(GetParameterFloat("angth"));
        lsdFilter->SetLogEps(GetParameterFloat("logeps"));
        lsdFilter->SetDensityTh(GetParameterFloat("densityth"));
        lsdFilter->SetNBins(GetParameterInt("nbins"));

        ExtractROIFilterType::Pointer imageROI = ExtractROIFilterType::New();

        unsigned long startX = column*sizeTilesX;
        unsigned long startY = row*sizeTilesY;
        unsigned long sizeX = vcl_min(sizeTilesX,sizeImageX-startX);
        unsigned long sizeY = vcl_min(sizeTilesY,sizeImageY-startY);

        // Tiles extraction of the segmented image
        imageROI->SetInput(imageIn);
        imageROI->SetStartX(startX);
        imageROI->SetStartY(startY);
        imageROI->SetSizeX(sizeX);
        imageROI->SetSizeY(sizeY);
        imageROI->SetChannel(1);
        imageROI->Update();

        // Detect segments on current tile
        lsdFilter->SetInput(imageROI->GetOutput());
        lsdFilter->Update();

        // Extract detected segment and convert them to OGR format
        int currentAddedSegments = 0;
        TreeIteratorType itVectorRef(lsdFilter->GetOutput()->GetDataTree());
        for(itVectorRef.GoToBegin(); !itVectorRef.IsAtEnd(); itVectorRef++) {
          if(itVectorRef.Get()->IsLineFeature()) {
            OGRLineString currentSegment;

            typename VertexListType::Pointer vlist = itVectorRef.Get()->GetLine()->GetVertexList();
            typename VertexListType::ConstIterator it = vlist->Begin();
            for(; it != vlist->End(); it++){
              VertexType currentVertex = it.Value();
              currentSegment.addPoint(currentVertex[0], currentVertex[1]);
            }
            detectedSegments.push_back(currentSegment);
            currentAddedSegments++;
          }
        }

        totalNbSegments += currentAddedSegments;
      }
    }

    // Write shapefile
    for(std::list<OGRLineString>::iterator it = detectedSegments.begin(); it != detectedSegments.end(); it++) {
      otb::ogr::Feature newFeature(layer.GetLayerDefn());
      newFeature.SetGeometry(&(*it));
      layer.CreateFeature(newFeature);
    }
    ogrDS->SyncToDisk();

    otbAppLogINFO(<<"Progress: 100% ("<<totalNbSegments<<" found)");
  }
};

} // namespace Wrapper
} // namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::FastLineSegmentDetection)
