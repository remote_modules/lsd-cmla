# FastLineSegmentDetection
## Synopsis

This code implements the algorithm for fast line segment detection.
This method gives subpixel accurate results. It is designed to work on any digital image without parameter tuning. 
It controls its own number of false detections: On average, one false alarms is allowed per image. 
The method is based on Burns, Hanson, and Riseman's method, and uses an a-contrario validation approach according to Desolneux, Moisan, and Morel's theory. 
The version described here includes some further improvement over the one described in the original article (see reference below).

![Example of line segment detection](iarpa_lsd.png?raw=true)

## Installation

FastSegmentDetection relies on Orfeo ToolBox and uses CMake (http://www.cmake.org) for building from source.

## Dependencies

Following a summary of the required dependencies: 

* GDAL >= 3.1
* OTB >= 7.2
* Python interpreter >= 3.7.2
* Python libs >= 3.7.2

GDAL itself depends on a number of other libraries provided by most major operating systems and also depends on the non standard GEOS and Proj libraries. GDAL- Python bindings are also required

## Tests

Enable tests with BUILD_TESTING cmake option. Use `ctest` command to run tests. Do not forget to clean your output test directory when you run a new set of tests.

For test purpose, the variable `TEST_DATA_ROOT` must be set within cmake.

A dataset archive is available [here](https://gitlab.orfeo-toolbox.org/remote_modules/image-to-db-registration/-/wikis/Test-data-for-the-remote-module)


## Contributors
Fast line segment detector algorithm : 
Rafael Grompone von Gioi1, Jérémie Jakubowicz, Jean-Michel Morel, Gregory Randall
See : Gioi, R., Jakubowicz, J., Morel, J.-M., Randall, G., 2010.
LSD: A Fast Line Segment Detector with a False Detection Control.
IEEE transactions on pattern analysis and machine intelligence, 32, 722-32.

OTB integration as a remote module :
Julien Michel (CNES), Yannick Tanguy (CNES), Rémi Demortier (Thales)
